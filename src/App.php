<?php

use Router\Router;

class App
{
    /** @var array */
    protected static $config;
    /** @var ServiceLocator */
    protected static $serviceLocator;
    
    /**
     * @param array $config
     */
    public static function init(array $config = null)
    {
        self::$config = $config;

        // Set a global BASE_PATH constant as the dir this index.php file lives in
        define('BASE_PATH', realpath(__DIR__ . '/..'));

        if (!empty($config['php_ini'])) {
            foreach ($config['php_ini'] as $key => $value) {
                ini_set($key, $value);
            }
        }

        spl_autoload_register(function($className) {
            // Convert class name to a filename based on PSR-0 convention
            $filename = BASE_PATH . '/src/';
            $filename .= str_replace('\\', '/', $className);
            $filename .= '.php';

            if (file_exists($filename)) {
                include $filename;
            }
        });

        $config = (!empty(self::$config['service_locator'])) ? self::$config['service_locator'] : [];
        self::$serviceLocator = new ServiceLocator($config);
    }

    /**
     * @return string
     */
    public static function run()
    {
        /** @var Router $router */
        $router = self::$serviceLocator->get('Router');
        $routeMatch = $router->route($_SERVER['REQUEST_URI']);
        if (!$routeMatch) {
            header("HTTP/1.0 404 Not Found");
            die('Page not found: Our custom router could not match the given URL.');
        }
        $controllerName = $routeMatch->getController();
        $actionName = $routeMatch->getAction();
        $args = $routeMatch->getArgs();

        $controller = self::$serviceLocator->get($controllerName);
        return $controller->$actionName($args);
    }

    /**
     * @return ServiceLocator
     */
    public static function getServiceLocator()
    {
        return self::$serviceLocator;
    }

}