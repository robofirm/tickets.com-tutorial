<?php

namespace Controller;

use TemplateRenderer;

class ArtistController
{
    /** @var TemplateRenderer */
    protected $templateRenderer;

    public function __construct(TemplateRenderer $templateRenderer)
    {
        $this->templateRenderer = $templateRenderer;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function indexAction()
    {
        return $this->templateRenderer->render('artist/index');
    }

    /**
     * @param array $data
     *
     * @return string
     * @throws \Exception
     */
    public function viewAction(array $data)
    {
        return $this->templateRenderer->render('artist/view', $data);
    }
}