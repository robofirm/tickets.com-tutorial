<?php

namespace Controller;

use App;
use TemplateRenderer;

class IndexController
{
    /** @var TemplateRenderer */
    protected $templateRenderer;

    public function __construct(TemplateRenderer $templateRenderer)
    {
        $this->templateRenderer = $templateRenderer;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function indexAction()
    {
        return $this->templateRenderer->render('index/index');
    }
}