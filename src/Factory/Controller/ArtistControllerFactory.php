<?php

namespace Factory\Controller;

use Controller\ArtistController;
use Factory\FactoryInterface;
use ServiceLocatorInterface;

class ArtistControllerFactory implements FactoryInterface
{

    public function create(ServiceLocatorInterface $serviceLocator)
    {
        /** @var \TemplateRenderer $templateRenderer */
        $templateRenderer = $serviceLocator->get('TemplateRenderer');
        return new ArtistController($templateRenderer);
    }
}