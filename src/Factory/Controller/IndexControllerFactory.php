<?php

namespace Factory\Controller;

use Controller\IndexController;
use Factory\FactoryInterface;
use ServiceLocatorInterface;

class IndexControllerFactory implements FactoryInterface
{

    public function create(ServiceLocatorInterface $serviceLocator)
    {
        /** @var \TemplateRenderer $templateRenderer */
        $templateRenderer = $serviceLocator->get('TemplateRenderer');
        return new IndexController($templateRenderer);
    }
}