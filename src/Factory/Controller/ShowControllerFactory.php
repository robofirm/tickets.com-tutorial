<?php

namespace Factory\Controller;

use Controller\ShowController;
use Factory\FactoryInterface;
use ServiceLocatorInterface;

class ShowControllerFactory implements FactoryInterface
{

    public function create(ServiceLocatorInterface $serviceLocator)
    {
        /** @var \TemplateRenderer $templateRenderer */
        $templateRenderer = $serviceLocator->get('TemplateRenderer');
        return new ShowController($templateRenderer);
    }
}