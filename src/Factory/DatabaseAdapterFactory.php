<?php

namespace Factory;

use ServiceLocatorInterface;

class DatabaseAdapterFactory implements FactoryInterface
{
    public function create(ServiceLocatorInterface $serviceLocator)
    {
        $dsn = 'mysql:host=localhost;dbname=ticketsite';
        $username = 'dev';
        $password = 'password1';
        return new \PDO($dsn, $username, $password);
    }
}