<?php

namespace Factory\Mapper;

use Mapper\ArtistMapper;
use Factory\FactoryInterface;
use PDO;
use ServiceLocatorInterface;

class ArtistMapperFactory implements FactoryInterface
{

    public function create(ServiceLocatorInterface $serviceLocator)
    {
        /** @var PDO $dbAdapter */
        $dbAdapter = $serviceLocator->get('DatabaseAdapter');
        return new ArtistMapper($dbAdapter);
    }
}