<?php

namespace Factory\Mapper;

use Mapper\ShowMapper;
use Factory\FactoryInterface;
use PDO;
use ServiceLocatorInterface;

class ShowMapperFactory implements FactoryInterface
{

    public function create(ServiceLocatorInterface $serviceLocator)
    {
        /** @var PDO $dbAdapter */
        $dbAdapter = $serviceLocator->get('DatabaseAdapter');
        return new ShowMapper($dbAdapter);
    }
}