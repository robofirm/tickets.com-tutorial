<?php

namespace Mapper;

use PDO;

class ArtistMapper
{
    /** @var PDO */
    protected $db;

    /**
     * ArtistMapper constructor.
     * @param PDO $dbAdapter
     */
    public function __construct(PDO $dbAdapter)
    {
        $this->db = $dbAdapter;
    }

    /**
     * @return array
     */
    public function getPopularArtists()
    {
        $stmt = $this->db->query("SELECT * FROM `artist` ORDER BY `popularity` DESC LIMIT 5");
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @return array
     */
    public function getAllArtists()
    {
        $stmt = $this->db->query("SELECT * FROM `artist` ORDER BY `name`");
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param int $id
     *
     * @return array|bool
     */
    public function getArtist($id)
    {
        $stmt = $this->db->query("SELECT * FROM `artist` WHERE `id` = " . $this->db->quote($id));
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if ($rows) {
            // Fake shows for now
            $rows[0]['shows'] = [
                ['name' => 'Show #1'],
                ['name' => 'Show #2'],
                ['name' => 'Show #3'],
            ];
            return $rows[0];
        }
        return false;
    }
}