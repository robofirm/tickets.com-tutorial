<?php

namespace Router;

class Routematch
{
    /** @var string */
    protected $controller;
    /** @var string */
    protected $action;
    /** @var array */
    protected $args;

    /**
     * Routematch constructor.
     *
     * @param $options
     */
    public function __construct(array $options = [])
    {
        if (isset($options['controller'])) {
            $this->controller = $options['controller'];
        }

        if (isset($options['action'])) {
            $this->action = $options['action'];
        }

        if (isset($options['args'])) {
            $this->args = $options['args'];
        }
    }

    /**
     * @return string|null
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * @return string|null
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @return array|null
     */
    public function getArgs()
    {
        return $this->args;
    }
}