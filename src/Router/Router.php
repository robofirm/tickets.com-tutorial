<?php

namespace Router;

use Mapper\UrlAliasMapper;

class Router
{
    /** @var \ServiceLocatorInterface */
    protected $serviceLocator;
    /** @var UrlAliasMapper */
    protected $urlAliasMapper;

    public function __construct(\ServiceLocatorInterface $serviceLocator, UrlAliasMapper $urlAliasMapper)
    {
        $this->serviceLocator = $serviceLocator;
        $this->urlAliasMapper = $urlAliasMapper;
    }

    /**
     * @param $requestUri
     *
     * @return bool|Routematch
     */
    public function route($requestUri)
    {
        $controller = $action = $args = null;

        if ($urlAlias = $this->urlAliasMapper->getUrlAliasByRequestUri($requestUri)) {
            $controller = $urlAlias['controller'];
            $action = $urlAlias['action'];
            if ($urlAlias['args']) {
                $args = json_decode($urlAlias['args'], true);
            }
        } else {
            $matches = [];
            switch ($requestUri) {
                case '/':
                    $controller = 'Controller\IndexController';
                    $action = 'indexAction';
                    break;

                case '/artists':
                    $controller = 'Controller\ArtistController';
                    $action = 'indexAction';
                    break;

                case '/shows':
                    $controller = 'Controller\ShowController';
                    $action = 'indexAction';
                    break;

                case 1 == preg_match('%/artist/(\d+)%', $requestUri, $matches):
                    $controller = 'Controller\ArtistController';
                    $action = 'viewAction';
                    $args['id'] = $matches[1];
                    break;

                case 1 == preg_match('%/show/(\d+)%', $requestUri, $matches):
                    $controller = 'Controller\ShowController';
                    $action = 'viewAction';
                    $args['id'] = $matches[1];
                    $args['id'] = $matches[1];
                    break;

                default:
                    return false;
            }
        }

        $controllerObj = $this->serviceLocator->get($controller);
        if (!is_callable([$controllerObj, $action])) {
            return false;
        }

        return new Routematch([
            'controller' => $controller,
            'action' => $action,
            'args' => $args,
        ]);
    }
}