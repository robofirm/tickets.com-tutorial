<?php

class TemplateRenderer
{
    /**
     * @param string $template
     * @param array  $data
     *
     * @return string
     * @throws Exception if template file does not exist
     */
    public function render($template, array $data = [])
    {
        $filename = BASE_PATH . "/views/$template.phtml";
        // Sanity check to ensure template file exists
        if (!file_exists($filename)) {
            throw new Exception("File \"$filename\" does not exist.");
        }
        // Extract data array in variables local to this method
        extract($data);

        // Start output buffering
        ob_start();

        // Require the template file. Variables within this method's scope can be used by this template.
        require($filename);

        // End output buffering and return the result of rendering the template
        return ob_get_clean();
    }
}